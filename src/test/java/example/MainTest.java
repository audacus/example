package example;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class MainTest {

    private Main main;

    @BeforeEach
    public void setUp() {
        this.main = new Main();
    }

    @Test
    public void calculateParity() {
        // odd
        assertEquals(Parity.ODD, main.calculateParity(5));
        assertEquals(true, main.isOdd(3));
        assertEquals(false, main.isOdd(4));
        // even
        assertEquals(Parity.EVEN, main.calculateParity(6));
        assertEquals(true, main.isEven(2));
        assertEquals(false, main.isEven(3));
    }

    @Test
    public void reverseString() {
        assertEquals("drowssorC", main.reverse("Crossword"));
        assertEquals("racecar", main.reverse("racecar"));
    }

    @Test
    public void isStringPalindrome() {
        assertEquals(true, main.isPalindrome("Racecar"));
        assertEquals(false, main.isPalindrome("Schmetterling"));
    }

    @Test
    public void simplifyString() {
        assertEquals("simplifiedstring", main.simplify("Simplified string"));
        assertEquals("easy", main.simplify("E             a            S       y"));
    }

    @Test
    public void isStringAnagram() {
        assertEquals(true, main.isAnagram("New York Times", "monkeys write"));
        assertEquals(true, main.isAnagram("a gentleman", "elegant man"));
        assertEquals(true, main.isAnagram("funeral", "real fun"));
        assertEquals(true, main.isAnagram("love", "velo"));
        assertEquals(false, main.isAnagram("love", "fahrrad"));
    }
}
