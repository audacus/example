package example;

import java.util.Arrays;

public class Main {

    public Parity calculateParity(int number) {
        return number % 2 == 0 ? Parity.EVEN : Parity.ODD;
    }

    public boolean isOdd(int number) {
        return this.calculateParity(number) == Parity.ODD;
    }

    public boolean isEven(int number) {
        return this.calculateParity(number) == Parity.EVEN;
    }

    public String reverse(String string) {
        return new StringBuilder(string).reverse().toString();
    }

    public boolean isPalindrome(String string) {
        return string.equalsIgnoreCase(this.reverse(string));
    }

    public String simplify(String string) {
        return string.toLowerCase().replaceAll("\\s", "");
    }

    public boolean isAnagram(String string1, String string2) {
        var chars1 = this.simplify(string1).toCharArray();
        var chars2 = this.simplify(string2).toCharArray();

        // cannot be anagram if has different amount of chars
        if (chars1.length != chars2.length) return false;

        Arrays.sort(chars1);
        Arrays.sort(chars2);
        return Arrays.equals(chars1, chars2);
    }
}
